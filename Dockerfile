FROM adoptopenjdk:11-jdk-hotspot as builder
COPY mvnw .
COPY .mvn .mvn
COPY pom.xml .
COPY src src
RUN ./mvnw install

ENV JAR_FILE=target/*.jar
#COPY ${JAR_FILE} application.jar
RUN cp ${JAR_FILE} application.jar
RUN java -Djarmode=layertools -jar application.jar extract

FROM adoptopenjdk:11-jre-hotspot
COPY --from=builder dependencies/ ./
# https://github.com/moby/moby/issues/37965 RUN true because i'm having same trouble of this issue unsolved yet
RUN true
COPY --from=builder snapshot-dependencies/ ./
RUN true
COPY --from=builder spring-boot-loader/ ./
RUN true
COPY --from=builder application/ ./
RUN true
COPY wait-for-it.sh wait-for-it.sh
RUN chmod +x wait-for-it.sh
