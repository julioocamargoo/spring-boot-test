package com.julioocamargoo.springboottest.domain.customer;

import com.julioocamargoo.springboottest.infra.ModelMapper;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.Period;
import java.util.Optional;

@Component
public class CustomerMapper implements ModelMapper<Customer, CustomerDto> {

    @Override
    public CustomerDto toDto(Customer entity) {
        CustomerDto dto = new CustomerDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setCpf(entity.getCpf());
        dto.setBirthday(entity.getBirthday());
        Optional.ofNullable(entity.getBirthday())
                .ifPresent(birthday -> dto.setAge(Period.between(birthday, LocalDate.now()).getYears()));
        return dto;
    }

    @Override
    public Customer toEntity(CustomerDto dto, Customer baseEntity, boolean partialUpdate) {
        if (partialUpdate) {
            baseEntity.setId(Optional.ofNullable(dto.getId()).orElse(baseEntity.getId()));
            baseEntity.setName(Optional.ofNullable(dto.getName()).orElse(baseEntity.getName()));
            baseEntity.setCpf(Optional.ofNullable(dto.getCpf()).orElse(baseEntity.getCpf()));
            baseEntity.setBirthday(Optional.ofNullable(dto.getBirthday()).orElse(baseEntity.getBirthday()));
            return baseEntity;
        }

        baseEntity.setId(dto.getId());
        baseEntity.setName(dto.getName());
        baseEntity.setCpf(dto.getCpf());
        baseEntity.setBirthday(dto.getBirthday());
        return baseEntity;
    }

    public Customer toEntity(CustomerDto dto) {
        return this.toEntity(dto, new Customer(), false);
    }
}
