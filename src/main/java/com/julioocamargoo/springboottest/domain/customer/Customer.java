package com.julioocamargoo.springboottest.domain.customer;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class Customer {
    @Id
    @GeneratedValue
    private Long id;

    private String name;

    @Column(unique = true)
    private String cpf;

    @Column(name = "birthday", columnDefinition = "DATE")
    private LocalDate birthday;

    public Customer() {
        super();
    }

    public Customer(String name, String cpf) {
        super();
        this.name = name;
        this.cpf = cpf;
    }

    public Customer(Long id, String name, String cpf, LocalDate birthday) {
        super();
        this.id = id;
        this.name = name;
        this.cpf = cpf;
        this.birthday = birthday;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }
}
