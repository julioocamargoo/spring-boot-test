package com.julioocamargoo.springboottest.infra;

public interface ModelMapper<T, S> {
    public S toDto(T entity);

    public T toEntity(S dto, T baseEntity, boolean partialUpdate);
}
