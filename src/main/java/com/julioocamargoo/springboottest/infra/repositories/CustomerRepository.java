package com.julioocamargoo.springboottest.infra.repositories;

import com.julioocamargoo.springboottest.domain.customer.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
