package com.julioocamargoo.springboottest.application.resources;

import com.julioocamargoo.springboottest.application.services.CustomerService;
import com.julioocamargoo.springboottest.infra.exceptions.CustomerNotFoundException;
import com.julioocamargoo.springboottest.domain.customer.CustomerDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class CustomerResources {

    @Autowired
    private CustomerService customerService;

    private Logger logger = LoggerFactory.getLogger(CustomerResources.class);

    @GetMapping("/customers/{id}")
    public ResponseEntity<CustomerDto> retrieveCustomer(@PathVariable long id) {
        try {
            CustomerDto customer = this.customerService.retrieveCustomer(id);
            return ResponseEntity.ok(customer);
        } catch (CustomerNotFoundException customerNotFoundException) {
            logger.error(customerNotFoundException.getMessage());
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        }
    }

    @GetMapping("/customers")
    public ResponseEntity<Map<String, Object>> retrieveCustomers(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "3") int size,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String cpf
    ) {
        Map<String, Object> customers = this.customerService.retrieveCustomers(page, size, name, cpf);
        return ResponseEntity.ok(customers);
    }

    @PostMapping("/customers")
    public ResponseEntity<Object> createCustomer(@RequestBody CustomerDto customerDto) {
        try {
            return ResponseEntity
                    .created(this.customerService.createCustomer(customerDto))
                    .build();
        } catch (Exception e) {
            this.logger.error(e.getMessage());
            throw e;
        }
    }

    @DeleteMapping("/customers/{id}")
    public ResponseEntity<Object> deleteCustomer(@PathVariable long id) {
        this.customerService.deleteCustomer(id);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/customers/{id}")
    public ResponseEntity<Object> updateCustomer(@RequestBody CustomerDto customerDto, @PathVariable long id) {
        try {
            this.customerService.updateCustomer(customerDto, id);
            return ResponseEntity.noContent().build();
        } catch (CustomerNotFoundException customerNotFoundException) {
            logger.error(customerNotFoundException.getMessage());
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        }
    }

    @PatchMapping("/customers/{id}")
    public ResponseEntity<CustomerDto> updateCustomerPartially(@RequestBody CustomerDto customerDto, @PathVariable long id) {
        try {
            this.customerService.updateCustomerPartially(customerDto, id);
            return ResponseEntity.noContent().build();
        } catch (CustomerNotFoundException customerNotFoundException) {
            logger.error(customerNotFoundException.getMessage());
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        }
    }
}
