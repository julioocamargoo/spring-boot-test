package com.julioocamargoo.springboottest.application.services;

import com.julioocamargoo.springboottest.domain.customer.Customer;
import com.julioocamargoo.springboottest.domain.customer.CustomerDto;
import com.julioocamargoo.springboottest.domain.customer.CustomerMapper;
import com.julioocamargoo.springboottest.infra.exceptions.CustomerNotFoundException;
import com.julioocamargoo.springboottest.infra.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class CustomerService {
    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CustomerMapper customerMapper;

    public CustomerDto retrieveCustomer(long id) throws CustomerNotFoundException {
        Optional<Customer> customer = this.customerRepository.findById(id);
        if (customer.isEmpty()) {
            throw new CustomerNotFoundException("Customer Id: " + id);
        }

        return customer.map(c -> this.customerMapper.toDto(c)).get();
    }

    public Map<String, Object> retrieveCustomers(
            int page,
            int size,
            String name,
            String cpf
    ) {
        Pageable paging = PageRequest.of(page, size);

        Example example = Example.of(
                new Customer(name, cpf),
                ExampleMatcher
                        .matchingAll()
                        .withIgnoreCase()
                        .withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING)
        );

        Page<Customer> pagedCustomers = this.customerRepository.findAll(example, paging);
        List<CustomerDto> customersList = pagedCustomers.map(c -> this.customerMapper.toDto(c)).getContent();

        Map<String, Object> customersMap = new HashMap<>();
        customersMap.put("data", customersList);
        customersMap.put("currentPage", pagedCustomers.getNumber());
        customersMap.put("totalItems", pagedCustomers.getTotalElements());
        customersMap.put("totalPages", pagedCustomers.getTotalPages());

        return customersMap;
    }

    public URI createCustomer(CustomerDto customerDto) {
        Customer createdCustomer = this.customerRepository.save(this.customerMapper.toEntity(customerDto));

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(createdCustomer.getId()).toUri();

        return location;
    }

    public void deleteCustomer(long id) {
        this.customerRepository.deleteById(id);
    }

    public void updateCustomer(CustomerDto customerDto, long id) throws CustomerNotFoundException {
        Optional<Customer> customerOptional = this.customerRepository.findById(id);

        if (customerOptional.isEmpty())
            throw new CustomerNotFoundException("Customer Id: " + id);

        Customer customer = this.customerMapper.toEntity(customerDto, customerOptional.get(), false);
        customer.setId(id);
        this.customerRepository.save(customer);
    }

    public void updateCustomerPartially(CustomerDto customerDto, long id) throws CustomerNotFoundException {
        Optional<Customer> customerOptional = this.customerRepository.findById(id);

        if (customerOptional.isEmpty())
            throw new CustomerNotFoundException("Customer Id: " + id);

        Customer customer = this.customerMapper.toEntity(customerDto, customerOptional.get(), true);
        customer.setId(id);
        this.customerRepository.save(customer);
    }
}
