package com.julioocamargoo.springboottest.application.services;

import com.julioocamargoo.springboottest.domain.customer.Customer;
import com.julioocamargoo.springboottest.domain.customer.CustomerDto;
import com.julioocamargoo.springboottest.domain.customer.CustomerMapper;
import com.julioocamargoo.springboottest.infra.exceptions.CustomerNotFoundException;
import com.julioocamargoo.springboottest.infra.repositories.CustomerRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.util.Optional;

@SpringBootTest
public class CustomerServiceTest {

    @Mock
    private CustomerRepository customerRepository;

    @Mock
    private CustomerMapper customerMapper;

    @InjectMocks
    private CustomerService customerService;

    @Test
    public void testRetrieveCustomerWithSuccess() {
        Customer customer = new Customer();
        customer.setId(1L);
        customer.setName("Test");
        customer.setCpf("01234567890");
        customer.setBirthday(LocalDate.of(1994, 9, 22));

        CustomerDto customerDto = new CustomerDto();
        customerDto.setId(customer.getId());
        customerDto.setName(customer.getName());
        customerDto.setCpf(customer.getCpf());
        customerDto.setBirthday(customer.getBirthday());
        customerDto.setAge(25);

        Mockito.when(this.customerRepository.findById(1L))
                .thenReturn(Optional.ofNullable(customer));

        Mockito.when(this.customerMapper.toDto(customer))
                .thenReturn(customerDto);

        CustomerDto c = this.customerService.retrieveCustomer(1L);
        Assertions.assertThat(c.getId()).isEqualTo(customer.getId());
        Assertions.assertThat(c.getCpf()).isEqualTo(customer.getCpf());
    }

    @Test
    public void testRetrieveCustomerNotFound() {
        org.junit.jupiter.api.Assertions.assertThrows(
                CustomerNotFoundException.class,
                () -> this.customerService.retrieveCustomer(2L)
        );
    }
}
