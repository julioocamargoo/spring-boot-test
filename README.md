# Spring boot + mysql + docker
This application is a simple CRUD using DDD based architecture and all that spring boot provided.

## How to run
* Have docker installed
* Have port 8080 free (or change port on docker-compose.yml)
* `docker-compose up -d` on root folder
* Be fun!

Also you can use postman collection attached here for test routes

## TODO
* Write tests
* Think about convert application service layer in use cases.